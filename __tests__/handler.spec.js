// https://jestjs.io/docs/expect
// https://github.com/mohamedlotfe/unit-testing-api-nodejs-jest

const app = require("../server")
const supertest = require("supertest")

describe("Test Handlers", function () {
    test("GET /casino/get", async () => {
        await supertest(app).get("/casino/get")
        .expect(200)
        .then((response) => {
            expect(Array.isArray(response.body)).toBeTruthy()
        })
    })
})