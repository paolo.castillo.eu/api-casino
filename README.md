# api-casino

API para el sistema de casino, en lo cual se esta realizando para la emigración del sistema en PHP a NODEJS en el backEnd y FrontEnd se realzara en su interfaz ReactJS. Los requesito a utilizar en el sistema es:

- body-parser
- cors
- dotenv
- express
- express-validator
- helmet 
- morgan
- oracledb

## Arquitectura del Proyecto:

La arquitectura a utilizar es cliente servidor separando los proyectos en FrondEnd con ReactJS y NodeJS en el BackEnd, y en la DB Oracle

![Alt text](arquitectura.png?raw=true "Title")

## Configuraciones

Para iniciar el proyecto se debe realizar la creación del archivo `.env` en la raiz del proyecto

```
DB_USER=dbuser
DB_PASSWORD=dbpass
DB_CONNECTIONSTRING="ip/sid"
PORT=port
```

## Problemas al instalar por proxy

En el caso de tener problema con el proxy local del equipo a instalar el sistema se debe configurar lo siguiente elementos:

```
$ # SET HTTPS Firebase
$ set "NODE_TLS_REJECT_UNAUTHORIZED=0"
$ npm config set strict-ssl false
```
