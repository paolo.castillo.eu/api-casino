const db = require(__dirDBOracle)
const oracledb = require('oracledb')
const control_casino = require(__dirDB)

const get = async (req, res) => {
    let rnum = `WHERE RNUM > 10 * (${req.query.page} - 1)`

    let est = ((req.query.estado) ? `AND TICKET.ID_ESTADO = ${req.query.estado}` : '')
    let cas = ((req.query.casino) ? `AND TICKET.ID_CASINO = ${req.query.casino}` : '')
    let col = ((req.query.colacion) ? `AND TICKET.ID_COLACION = ${req.query.colacion}` : '')
    let emp = ((req.query.colaborador) ? `AND TICKET.ID_COLABORADOR = ${req.query.colaborador}` : '')

    db.dbSelectResult(
        control_casino.db_casino,
        `SELECT *
                FROM (SELECT INNER_QUERY.*, ROWNUM RNUM
                        FROM (  SELECT TICKET.ID_TICKET,
                                        TICKET.ID_COLABORADOR,
                                        COLABORADOR.CODIGO
                                            RUT,
                                        COLABORADOR.NOMBRE
                                            COLABORADOR,
                                        COLABORADOR.CREDENCIAL,
                                        TICKET.ID_ESTADO,
                                        ESTADO.NOMBRE
                                            ESTADO,
                                        TICKET.ID_CASINO,
                                        CASINO.NOMBRE
                                            CASINO,
                                        TICKET.ID_COLACION,
                                        COLACION.NOMBRE
                                            COLACION,
                                        TICKET.CORRELATIVO,
                                        TICKET.HORA_ENTREGA,
                                        TO_CHAR(TICKET.FECHA_ENTREGA, 'dd-mm-rrrr')
                                            FECHA_ENTREGA,
                                        TICKET.RUT_EMP,
                                        TO_CHAR(TICKET.FECHA_COLACION, 'dd-mm-rrrr')
                                            FECHA_COLACION,
                                        TICKET.ID_SOLICITUD
                                    FROM PFSC_TICKET TICKET
                                        INNER JOIN PFSC_COLABORADOR COLABORADOR
                                            ON TICKET.ID_COLABORADOR =
                                                COLABORADOR.ID_COLABORADOR
                                        INNER JOIN PFSC_CASINO CASINO
                                            ON TICKET.ID_CASINO = CASINO.ID_CASINO
                                        INNER JOIN PFSC_COLACION COLACION
                                            ON TICKET.ID_COLACION = COLACION.ID_COLACION
                                        INNER JOIN PFSC_ESTADO_TICKET ESTADO
                                            ON TICKET.ID_ESTADO = ESTADO.ID_ESTADO
                                WHERE	 1 = 1
                                        AND TICKET.FECHA_ENTREGA BETWEEN TO_DATE( :DESDE,
                                                                                'dd-mm-rrrr')
                                                                    AND TO_DATE( :HASTA,
                                                                                'dd-mm-rrrr')
                                        ${est} ${cas} ${col} ${emp}
                                ORDER BY TICKET.ID_TICKET ASC) INNER_QUERY
                        WHERE ROWNUM <= :PAGE * 10) ${rnum}`,
        [
            req.query.desde,
            req.query.hasta,
            req.query.page,
        ],
        res)
}

const getTicket = async (req, res) => {
    db.dbCursor(
        control_casino.db_casino,
        `BEGIN
                PF_SYS_CASINO.GET_TICKET(:CURSOR);
            END;`,
        {
            CURSOR: { type: oracledb.CURSOR, dir: oracledb.BIND_OUT }
        },
        res)
}

const getTotalTicket = async (req, res) => {
    let rnum = `WHERE RNUM > 10 * (${req.query.page} - 1)`

    let est = ((req.query.estado) ? `AND TIK.ID_ESTADO = ${req.query.estado}` : '')
    let cas = ((req.query.casino) ? `AND TIK.ID_CASINO = ${req.query.casino}` : '')
    let col = ((req.query.colacion) ? `AND TIK.ID_COLACION = ${req.query.colacion}` : '')
    let emp = ((req.query.colaborador) ? `AND TIK.ID_COLABORADOR = ${req.query.colaborador}` : '')

    db.dbSelectResult(
        control_casino.db_casino,
        `SELECT *
                FROM (SELECT INNER_QUERY.*, ROWNUM RNUM
                        FROM (SELECT 
                                    PLA.NOMBRE AS PLANTA,
                                    CAS.NOMBRE AS CASINO,
                                    COL.NOMBRE AS COLACION,
                                    EST.NOMBRE AS ESTADO,
                                    COUNT(TIK.ID_TICKET) AS TICKET
                                FROM PFSC_TICKET TIK
                                JOIN PFSC_CASINO CAS ON TIK.ID_CASINO = CAS.ID_CASINO
                                JOIN PFSC_PLANTA PLA ON CAS.ID_PLANTA = PLA.ID_PLANTA
                                JOIN PFSC_COLACION COL ON TIK.ID_COLACION = COL.ID_COLACION
                                JOIN PFSC_ESTADO_TICKET EST ON TIK.ID_ESTADO = EST.ID_ESTADO
                                WHERE TIK.FECHA_ENTREGA BETWEEN
                                TO_DATE(:PDESDE,'DD/MM/YYYY') AND TO_DATE(:PHASTA,'DD/MM/YYYY')
                                ${est} ${cas} ${col} ${emp}
                                GROUP BY PLA.NOMBRE, CAS.NOMBRE, COL.NOMBRE, EST.NOMBRE
                                ) INNER_QUERY
                        WHERE ROWNUM <= :PAGE * 10) ${rnum}`,
        [
            req.query.desde,
            req.query.hasta,
            req.query.page,
        ],
        res)
}

const getTotalPorColaborador = async (req, res) => {
    let rnum = `WHERE RNUM > 10 * (${req.query.page} - 1)`

    let est = ((req.query.estado) ? `AND TIK.ID_ESTADO = ${req.query.estado}` : '')
    let cas = ((req.query.casino) ? `AND TIK.ID_CASINO = ${req.query.casino}` : '')
    let col = ((req.query.colacion) ? `AND TIK.ID_COLACION = ${req.query.colacion}` : '')
    let emp = ((req.query.colaborador) ? `AND TIK.ID_COLABORADOR = ${req.query.colaborador}` : '')

    db.dbSelectResult(
        control_casino.db_casino,
        `SELECT *
                FROM (SELECT INNER_QUERY.*, ROWNUM RNUM
                        FROM (SELECT 
                                    TIK.ID_TICKET,
                                    TIK.CORRELATIVO,
                                    COLA.CODIGO AS RUT,
                                    COLA.NOMBRE, 
                                    COLA.CREDENCIAL,
                                    COLA.DEPARTAMENTO,
                                    TO_CHAR(TIK.FECHA_ENTREGA,'DD-MM-RRRR') AS FECHA_ENTREGA, 
                                    TIK.HORA_ENTREGA, 	 
                                    EST.NOMBRE AS ESTADO
                                FROM PFSC_TICKET TIK
                                JOIN PFSC_COLABORADOR COLA ON TIK.ID_COLABORADOR = COLA.ID_COLABORADOR
                                JOIN PFSC_CASINO CAS ON TIK.ID_CASINO = CAS.ID_CASINO
                                JOIN PFSC_PLANTA PLA ON CAS.ID_PLANTA = PLA.ID_PLANTA
                                JOIN PFSC_COLACION COL ON TIK.ID_COLACION = COL.ID_COLACION
                                JOIN PFSC_ESTADO_TICKET EST ON TIK.ID_ESTADO = EST.ID_ESTADO
                                WHERE TIK.FECHA_ENTREGA BETWEEN
                                TO_DATE(:PDESDE,'DD/MM/YYYY') AND TO_DATE(:PHASTA,'DD/MM/YYYY')
                                ${est} ${cas} ${col} ${emp}
                                ORDER BY TIK.ID_TICKET ASC) INNER_QUERY
                        WHERE ROWNUM <= :PAGE * 10) ${rnum}`,
        [
            req.query.desde,
            req.query.hasta,
            req.query.page,
        ],
        res)

}

module.exports = {
    get,
    getTicket,
    getTotalTicket,
    getTotalPorColaborador
}