const db = require(__dirDBOracle)
const control_casino = require(__dirDB)

const get = async (req, res) => {
    db.dbSelectResult( 
        control_casino.db_casino,
        `SELECT 
            CAS.ID_CASINO,
            CAS.ESTADO,
            CAS.CAPACIDAD,
            CAS.CORREO_ADMIN,
            CAS.CORREO_ADMIN_PF,
            CAS.NOMBRE,
            CAS.TERMINALES,
            CAS.ID_PLANTA,
            PLA.NOMBRE	  PLANTA,
            CAS.FLAG_VALIDA_MARCA,
            CAS.CORREO_NOTIF_ADM,
            CAS.CORREO_NOTIF_RRHH,
            CAS.CORREO_NOTIF_SEGURIDAD,
            CAS.FLAG_ENVIA_MARCA,
            CAS.CORREO_NOTIF_MARCA
        FROM PFSC_CASINO	CAS
            INNER JOIN PFSC_PLANTA PLA ON CAS.ID_PLANTA = PLA.ID_PLANTA`,
        [], 
        res)
}

const getBy = async (req, res) => {
    db.dbSelectResult(
        control_casino.db_casino,
        `SELECT CAS.ID_CASINO,
                CAS.ESTADO,
                CAS.CAPACIDAD,
                CAS.CORREO_ADMIN,
                CAS.CORREO_ADMIN_PF,
                CAS.NOMBRE,
                CAS.TERMINALES,
                CAS.ID_PLANTA,
                PLA.NOMBRE	  PLANTA,
                CAS.FLAG_VALIDA_MARCA,
                CAS.CORREO_NOTIF_ADM,
                CAS.CORREO_NOTIF_RRHH,
                CAS.CORREO_NOTIF_SEGURIDAD,
                CAS.FLAG_ENVIA_MARCA,
                CAS.CORREO_NOTIF_MARCA
            FROM PFSC_CASINO	CAS
                INNER JOIN PFSC_PLANTA PLA ON CAS.ID_PLANTA = PLA.ID_PLANTA
            WHERE CAS.ID_CASINO = :idbv`,
        [req.params.id],
        res)
}

const upd = async (req, res) => {
    db.dbCrud(
        control_casino.db_casino,
        `UPDATE PFSC_CASINO
                SET ESTADO = :DV_ESTADO,
                    CAPACIDAD = :DV_CAPACIDAD,
                    CORREO_ADMIN = :DV_CORREO_ADMIN,
                    CORREO_ADMIN_PF = :DV_CORREO_ADMIN_PF,
                    NOMBRE = :DV_NOMBRE,
                    TERMINALES = :DV_TERMINALES,
                    ID_PLANTA = :DV_ID_PLANTA,
                    MODIFICADO_POR = :DV_MODIFICADO_POR,
                    FECHA_MODIFICACION = SYSDATE,
                    FLAG_VALIDA_MARCA = :DV_FLAG_VALIDA_MARCA,
                    CORREO_NOTIF_ADM = :DV_CORREO_NOTIF_ADM,
                    CORREO_NOTIF_RRHH = :DV_CORREO_NOTIF_RRHH,
                    CORREO_NOTIF_SEGURIDAD = :DV_CORREO_NOTIF_SEGURIDAD,
                    FLAG_ENVIA_MARCA = :DV_FLAG_ENVIA_MARCA,
                    CORREO_NOTIF_MARCA = :DV_CORREO_NOTIF_MARCA
                WHERE ID_CASINO = :DV_ID_CASINO`, 
        [
            req.body.estado,
            req.body.capacidad,
            req.body.correo_admin,
            req.body.correo_admin_pf,
            req.body.nombre,
            req.body.terminales,
            req.body.id_planta,
            req.body.modificado_por,
            req.body.flag_valida_marca,
            req.body.correo_notif_adm,
            req.body.correo_notif_rrhh,
            req.body.correo_notif_seguridad,
            req.body.flag_envia_marca,
            req.body.correo_notif_marca,
            req.body.id_casino
        ], res)
}

const del = async (req, res) => {
    db.dbCrud(
        control_casino.db_casino,
        `DELETE PFSC_CASINO
                WHERE ID_CASINO = :DV_ID_CASINO`,
        [
            req.body.id_casino
        ], res)
}

const add = async (req, res) => {
    db.dbCrud(
        control_casino.db_casino,
        `INSERT INTO PFSC_CASINO(ID_CASINO,
                                    ESTADO,
                                    CAPACIDAD,
                                    CORREO_ADMIN,
                                    CORREO_ADMIN_PF,
                                    NOMBRE,
                                    TERMINALES,
                                    ID_PLANTA,
                                    CREADO_POR,
                                    FECHA_CREACION,
                                    FLAG_VALIDA_MARCA,
                                    CORREO_NOTIF_ADM,
                                    CORREO_NOTIF_RRHH,
                                    CORREO_NOTIF_SEGURIDAD,
                                    FLAG_ENVIA_MARCA,
                                    CORREO_NOTIF_MARCA)
                VALUES (:DV_ID_CASINO,
                        :DV_ESTADO,
                        :DV_CAPACIDAD,
                        :DV_CORREO_ADMIN,
                        :DV_CORREO_ADMIN_PF,
                        :DV_NOMBRE,
                        :DV_TERMINALES,
                        :DV_ID_PLANTA,
                        :DV_CREADO_POR,
                        SYSDATE,
                        :DV_FLAG_VALIDA_MARCA,
                        :DV_CORREO_NOTIF_ADM,
                        :DV_CORREO_NOTIF_RRHH,
                        :DV_CORREO_NOTIF_SEGURIDAD,
                        :DV_FLAG_ENVIA_MARCA,
                        :DV_CORREO_NOTIF_MARCA)`,
        [
            req.body.id_casino,
            req.body.estado,
            req.body.capacidad,
            req.body.correo_admin,
            req.body.correo_admin_pf,
            req.body.nombre,
            req.body.terminales,
            req.body.id_planta,
            req.body.creado_por,
            req.body.flag_valida_marca,
            req.body.correo_notif_adm,
            req.body.correo_notif_rrhh,
            req.body.correo_notif_seguridad,
            req.body.flag_envia_marca,
            req.body.correo_notif_marca
        ], res)
}

module.exports = {
    get,
    getBy,
    upd,
    add,
    del
}