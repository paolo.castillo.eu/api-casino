const db = require(__dirDBOracle)
const control_casino = require(__dirDB)

const get = async (req, res) => {
    db.dbSelectResult(
        control_casino.db_casino,
        `SELECT ID_PERFIL,
                    ESTADO,
                    NOMBRE,
                    CREADO_POR,
                    FECHA_CREACION,
                    MODIFICADO_POR,
                    FECHA_MODIFICACION
                FROM PFSC_PERFIL`,
        [],
        res)
}

const getBy = async (req, res) => {
    db.dbSelectResult(
        control_casino.db_casino,
        `SELECT ID_PERFIL,
                    ESTADO,
                    NOMBRE,
                    CREADO_POR,
                    FECHA_CREACION,
                    MODIFICADO_POR,
                    FECHA_MODIFICACION
                FROM PFSC_PERFIL
                WHERE  ID_PERFIL = :ID_PERFIL`,
        [req.params.id],
        res)
}

module.exports = {
    get,
    getBy,
}