const db = require(__dirDBOracle)
const control_casino = require(__dirDB)

const get = async (req, res) => {
    db.dbSelectResult(
        control_casino.db_casino,
        `SELECT ID_TIPO_SERVICIO,
                    NOMBRE,
                    ESTADO,
                    CREADO_POR,
                    FECHA_CREACION,
                    MODIFICADO_POR,
                    FECHA_MODIFICACION
                FROM PFSC_TIPO_SERVICIO`,
        [],
        res)
}

const getBy = async (req, res) => {
    db.dbSelectResult(
        control_casino.db_casino,
        `SELECT ID_TIPO_SERVICIO,
                    NOMBRE,
                    ESTADO,
                    CREADO_POR,
                    FECHA_CREACION,
                    MODIFICADO_POR,
                    FECHA_MODIFICACION
                FROM PFSC_TIPO_SERVICIO
                WHERE ID_TIPO_SERVICIO = :ID_TIPO_SERVICIO`,
        [req.params.id],
        res)
}

const upd = async (req, res) => {
    db.dbCrud(
        control_casino.db_casino,
        `UPDATE PFSC_TIPO_SERVICIO
                SET NOMBRE = :NOMBRE,
                    MODIFICADO_POR = :MODIFICADO_POR, 
                    FECHA_MODIFICACION = SYSDATE
                WHERE ID_TIPO_SERVICIO = :ID_TIPO_SERVICIO`,
        [
            req.body.nombre,
            req.body.modificado_por,
            req.body.id_tipo_servicio
        ], res)
}

module.exports = {
    get,
    getBy,
    upd
}