const db = require(__dirDBOracle)
const control_casino = require(__dirDB)

const get = async (req, res) => {
    db.dbSelectResult(
        control_casino.db_casino,
        `SELECT ID_ESTADO,
                NOMBRE,
                ESTADO,
                CREADO_POR,
                FECHA_CREACION,
                MODIFICADO_POR,
                FECHA_MODIFICACION
            FROM PFSC_ESTADO_TICKET`,
        [],
        res)
}

const getBy = async (req, res) => {
    db.dbSelectResult(
        control_casino.db_casino,
        `SELECT ID_ESTADO,
                    NOMBRE,
                    ESTADO,
                    CREADO_POR,
                    FECHA_CREACION,
                    MODIFICADO_POR,
                    FECHA_MODIFICACION
                FROM PFSC_ESTADO_TICKET
                WHERE  ID_ESTADO = :ID_ESTADO`,
        [req.params.id],
        res)
}

module.exports = {
    get,
    getBy,
}