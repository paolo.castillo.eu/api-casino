const db = require(__dirDBOracle)
const control_casino = require(__dirDB)

const get = async (req, res) => {
    db.dbSelectResult(
        control_casino.db_casino,
        `SELECT COLACION.ID_COLACION,
                    COLACION.ID_CASINO,
                    CASINO.NOMBRE	 CASINO,
                    PLANTA.NOMBRE	 PLANTA,
                    COLACION.ESTADO,
                    COLACION.VERSION,
                    COLACION.COSTO,
                    COLACION.FECHA_VIGENCIA_INICIAL,
                    COLACION.FECHA_VIGENCIA_FINAL,
                    COLACION.CORTE_SOLICITUD,
                    COLACION.HORA_INICIO,
                    COLACION.HORA_FIN,
                    COLACION.NOMBRE,
                    COLACION.TIPO_COLACION,
                    COLACION.CREADO_POR,
                    COLACION.FECHA_CREACION,
                    COLACION.MODIFICADO_POR,
                    COLACION.FECHA_MODIFICACION,
                    COLACION.MINUTO_INICIO,
                    COLACION.MINUTO_CORTE,
                    COLACION.TOTAL_COLACION
                FROM PFSC_COLACION  COLACION
                    LEFT JOIN PFSC_CASINO CASINO ON COLACION.ID_CASINO = CASINO.ID_CASINO
                    LEFT JOIN PFSC_PLANTA PLANTA ON CASINO.ID_PLANTA = PLANTA.ID_PLANTA`,
        [],
        res)
}

const getBy = async (req, res) => {
    db.dbSelectResult(
        control_casino.db_casino,
        `SELECT COLACION.ID_COLACION,
                    COLACION.ID_CASINO,
                    CASINO.NOMBRE	 CASINO,
                    PLANTA.NOMBRE	 PLANTA,
                    COLACION.ESTADO,
                    COLACION.VERSION,
                    COLACION.COSTO,
                    COLACION.FECHA_VIGENCIA_INICIAL,
                    COLACION.FECHA_VIGENCIA_FINAL,
                    LPAD(CASE
						WHEN LENGTH(TO_CHAR(COLACION.CORTE_SOLICITUD)) = 1 THEN
							'00:00'
						WHEN LENGTH(TO_CHAR(COLACION.CORTE_SOLICITUD)) = 3 THEN
							TO_CHAR(SUBSTR(TO_CHAR(COLACION.CORTE_SOLICITUD),0,1)||':'||
							SUBSTR(TO_CHAR(COLACION.CORTE_SOLICITUD),2,2))
						WHEN LENGTH(TO_CHAR(COLACION.CORTE_SOLICITUD)) = 4 THEN
							TO_CHAR(SUBSTR(TO_CHAR(COLACION.CORTE_SOLICITUD),0,2)||':'||
							SUBSTR(TO_CHAR(COLACION.CORTE_SOLICITUD),3,2))
					END,5,'0') CORTE_SOLICITUD,
                    LPAD(CASE
						WHEN LENGTH(TO_CHAR(COLACION.HORA_INICIO)) = 1 THEN
							'00:00'
						WHEN LENGTH(TO_CHAR(COLACION.HORA_INICIO)) = 3 THEN
							TO_CHAR(SUBSTR(TO_CHAR(COLACION.HORA_INICIO),0,1)||':'||
							SUBSTR(TO_CHAR(COLACION.HORA_INICIO),2,2))
						WHEN LENGTH(TO_CHAR(COLACION.HORA_INICIO)) = 4 THEN
							TO_CHAR(SUBSTR(TO_CHAR(COLACION.HORA_INICIO),0,2)||':'||
							SUBSTR(TO_CHAR(COLACION.HORA_INICIO),3,2))
					END,5,'0') HORA_INICIO,
                    LPAD(CASE
						WHEN LENGTH(TO_CHAR(COLACION.HORA_INICIO)) = 1 THEN
							'00:00'
						WHEN LENGTH(TO_CHAR(COLACION.HORA_FIN)) = 3 THEN
							TO_CHAR(SUBSTR(TO_CHAR(COLACION.HORA_FIN),0,1)||':'||
							SUBSTR(TO_CHAR(COLACION.HORA_FIN),2,2))
						WHEN LENGTH(TO_CHAR(COLACION.HORA_FIN)) = 4 THEN
							TO_CHAR(SUBSTR(TO_CHAR(COLACION.HORA_FIN),0,2)||':'||
							SUBSTR(TO_CHAR(COLACION.HORA_FIN),3,2))
					END,5,'0') HORA_FIN,
                    COLACION.NOMBRE,
                    COLACION.TIPO_COLACION,
                    COLACION.CREADO_POR,
                    COLACION.FECHA_CREACION,
                    COLACION.MODIFICADO_POR,
                    COLACION.FECHA_MODIFICACION,
                    COLACION.MINUTO_INICIO,
                    COLACION.MINUTO_CORTE,
                    COLACION.TOTAL_COLACION
                FROM PFSC_COLACION  COLACION
                    LEFT JOIN PFSC_CASINO CASINO ON COLACION.ID_CASINO = CASINO.ID_CASINO
                    LEFT JOIN PFSC_PLANTA PLANTA ON CASINO.ID_PLANTA = PLANTA.ID_PLANTA
                WHERE  COLACION.ID_COLACION = :ID_COLACION`,
        [req.params.id],
        res)
}

const upd = async (req, res) => {
    db.dbCrud(
        control_casino.db_casino,
        `UPDATE PFSC_COLACION
                SET ID_CASINO = :ID_CASINO,
                    ESTADO = :ESTADO,
                    VERSION = :VERSION,
                    COSTO = :COSTO,
                    FECHA_VIGENCIA_INICIAL = :FECHA_VIGENCIA_INICIAL,
                    FECHA_VIGENCIA_FINAL = :FECHA_VIGENCIA_FINAL,
                    CORTE_SOLICITUD = :CORTE_SOLICITUD,
                    HORA_INICIO = :HORA_INICIO,
                    HORA_FIN = :HORA_FIN,
                    NOMBRE = :NOMBRE,
                    TIPO_COLACION = :TIPO_COLACION,
                    MODIFICADO_POR = :MODIFICADO_POR,
                    FECHA_MODIFICACION = SYSDATE,
                    MINUTO_INICIO = :MINUTO_INICIO,
                    MINUTO_CORTE = :MINUTO_CORTE,
                    TOTAL_COLACION = :TOTAL_COLACION
                WHERE ID_COLACION = :ID_COLACION`,
        [
            req.body.id_casino,
            req.body.estado,
            req.body.version,
            req.body.costo,
            req.body.fecha_vigencia_inicial,
            req.body.fecha_vigencia_final,
            req.body.corte_solicitud,
            req.body.hora_inicio,
            req.body.hora_fin,
            req.body.nombre,
            req.body.tipo_colacion,
            req.body.modificado_por,
            req.body.minuto_inicio,
            req.body.minuto_corte,
            req.body.total_colacion,
            req.body.id_colacion
        ], res)
}

const del = async (req, res) => {
    db.dbCrud(
        control_casino.db_casino,
        `DELETE PFSC_COLACION
                    WHERE	   ID_COLACION = :ID_COLACION`,
        [
            req.body.idcolaborador
        ], res)
}

const add = async (req, res) => {
    db.dbCrud(
        control_casino.db_casino,
        `INSERT INTO PFSC_COLACION( ID_COLACION,
                                        ID_CASINO,
                                        ESTADO,
                                        VERSION,
                                        COSTO,
                                        FECHA_VIGENCIA_INICIAL,
                                        FECHA_VIGENCIA_FINAL,
                                        CORTE_SOLICITUD,
                                        HORA_INICIO,
                                        HORA_FIN,
                                        NOMBRE,
                                        TIPO_COLACION,
                                        CREADO_POR,
                                        FECHA_CREACION,
                                        MINUTO_INICIO,
                                        MINUTO_CORTE,
                                        TOTAL_COLACION)
                    VALUES (:ID_COLACION,
                            :ID_CASINO,
                            :ESTADO,
                            :VERSION,
                            :COSTO,
                            :FECHA_VIGENCIA_INICIAL,
                            :FECHA_VIGENCIA_FINAL,
                            :CORTE_SOLICITUD,
                            :HORA_INICIO,
                            :HORA_FIN,
                            :NOMBRE,
                            :TIPO_COLACION,
                            :CREADO_POR,
                            :FECHA_CREACION,
                            :MINUTO_INICIO,
                            :MINUTO_CORTE,
                            :TOTAL_COLACION)`,
        [
            req.body.id_colacion,
            req.body.id_casino,
            req.body.estado,
            req.body.version,
            req.body.costo,
            req.body.fecha_vigencia_inicial,
            req.body.fecha_vigencia_final,
            req.body.corte_solicitud,
            req.body.hora_inicio,
            req.body.hora_fin,
            req.body.nombre,
            req.body.tipo_colacion,
            req.body.creado_por,
            req.body.fecha_creacion,
            req.body.minuto_inicio,
            req.body.minuto_corte,
            req.body.total_colacion,
        ], res)
}

module.exports = {
    get,
    getBy,
    upd,
    add,
    del
}