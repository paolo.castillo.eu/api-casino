const db = require(__dirDBOracle)
const control_casino = require(__dirDB)

const getBy = async (req, res) => {
    db.dbSelectResult(
        control_casino.db_casino,
        `SELECT SOLICITUD.ID_SOLICITUD,
                    SOLICITUD.ID_COLABORADOR,
                    COLABORADOR.CODIGO								   RUT,
                    COLABORADOR.NOMBRE								   COLABORADOR,
                    COLABORADOR.CREDENCIAL,
                    SOLICITUD.ESTADO 								   ID_ESTADO,
                    ESTADO.NOMBRE									   ESTADO,
                    SOLICITUD.ID_CASINO,
                    CASINO.NOMBRE									   CASINO,
                    PLANTA.NOMBRE									   PLANTA,
                    SOLICITUD.ID_COLACION,
                    COLACION.NOMBRE									   COLACION,
                    LPAD(CASE
						WHEN LENGTH(TO_CHAR(COLACION.HORA_INICIO)) = 1 THEN
							'00:00'
						WHEN LENGTH(TO_CHAR(COLACION.HORA_INICIO)) = 3 THEN
							TO_CHAR(SUBSTR(TO_CHAR(COLACION.HORA_INICIO),0,1)||':'||
							SUBSTR(TO_CHAR(COLACION.HORA_INICIO),2,2))
						WHEN LENGTH(TO_CHAR(COLACION.HORA_INICIO)) = 4 THEN
							TO_CHAR(SUBSTR(TO_CHAR(COLACION.HORA_INICIO),0,2)||':'||
							SUBSTR(TO_CHAR(COLACION.HORA_INICIO),3,2))
					END,5,'0')                                         HORA_INICIO,
                    LPAD(CASE
						WHEN LENGTH(TO_CHAR(COLACION.HORA_INICIO)) = 1 THEN
							'00:00'
						WHEN LENGTH(TO_CHAR(COLACION.HORA_FIN)) = 3 THEN
							TO_CHAR(SUBSTR(TO_CHAR(COLACION.HORA_FIN),0,1)||':'||
							SUBSTR(TO_CHAR(COLACION.HORA_FIN),2,2))
						WHEN LENGTH(TO_CHAR(COLACION.HORA_FIN)) = 4 THEN
							TO_CHAR(SUBSTR(TO_CHAR(COLACION.HORA_FIN),0,2)||':'||
							SUBSTR(TO_CHAR(COLACION.HORA_FIN),3,2))
					END,5,'0')                                         HORA_FIN,
                    SOLICITUD.ACTIVA,
                    SOLICITUD.NUM_TICKET_SOLICITADO,
                    SOLICITUD.NUM_TICKET_CONSUMIDO,
                    TO_CHAR(SOLICITUD.FECHA_COLACION, 'dd-mm-rrrr')	   FECHA_COLACION,
                    SOLICITUD.JUSTIFICACION,
                    SOLICITUD.CREADO_POR,
                    SOLICITUD.FECHA_CREACION,
                    SOLICITUD.MODIFICADO_POR,
                    SOLICITUD.FECHA_MODIFICACION
                FROM PFSC_SOLICITUD  SOLICITUD
                    INNER JOIN PFSC_COLABORADOR COLABORADOR
                        ON SOLICITUD.ID_COLABORADOR = COLABORADOR.ID_COLABORADOR
                    INNER JOIN PFSC_ESTADO_TICKET ESTADO
                        ON SOLICITUD.ESTADO = ESTADO.ID_ESTADO
                    INNER JOIN PFSC_CASINO CASINO
                        ON SOLICITUD.ID_CASINO = CASINO.ID_CASINO
                    INNER JOIN PFSC_PLANTA PLANTA ON CASINO.ID_PLANTA = PLANTA.ID_PLANTA
                    INNER JOIN PFSC_COLACION COLACION
                        ON SOLICITUD.ID_COLACION = COLACION.ID_COLACION
                WHERE SOLICITUD.ID_COLABORADOR = :ID_COLABORADOR`,
        [req.params.id],
        res)
}

const add = async (req, res) => {
    db.dbProcedure(
        control_casino.db_casino,
        `BEGIN
                PF_SYS_CASINO.APROBAR_SOLICITUD(:PID_SOLICITUD, :PRUT, :PMODIFICADO, :FLAG);
            END;`,
        {
            pid_solicitud: { val: req.body.id_solicitud, dir: oracledb.BIND_IN },
            prut: { val: req.body.rut, dir: oracledb.BIND_IN },
            pmodificado: { val: req.body.modificado, dir: oracledb.BIND_IN },
            flag: { type: oracledb.NUMBER, dir: oracledb.BIND_OUT },
        },
        res)
}

module.exports = {
    getBy,
    add
}