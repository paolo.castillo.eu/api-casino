const db = require(__dirDBOracle)
const control_casino = require(__dirDB)

const get = async (req, res) => {
    let rnum = `WHERE RNUM > 10 * (${req.params.page} - 1)`
    db.dbSelectResult(
        control_casino.db_casino,
        `SELECT *
                FROM (SELECT INNER_QUERY.*, ROWNUM RNUM
                        FROM (  SELECT CC.ID_COLABORADOR,
                                        CO.NOMBRE		NOMBRE_COLABORADOR,
                                        CC.ESTADO,
                                        CC.MAXTIPOCOLACION,
                                        CC.ID_CASINO,
                                        CA.NOMBRE		NOMBRE_CASINO,
                                        CA.ID_PLANTA,
                                        PA.NOMBRE		NOMBRE_PLANTA,
                                        CC.ID_COLACION,
                                        COL.NOMBRE 	NOMBRE_COLACION,
                                        CC.ID_TIPO_SERVICIO,
                                        SE.NOMBRE		NOMBRE_TIPO_SERVICIO,
                                        CC.CREADO_POR,
                                        CC.FECHA_CREACION,
                                        CC.MODIFICADO_POR,
                                        CC.FECHA_MODIFICACION
                                    FROM PFSC_COLABORADOR_COLACION CC
                                        INNER JOIN PFSC_COLABORADOR CO
                                            ON CC.ID_COLABORADOR = CO.ID_COLABORADOR
                                        INNER JOIN PFSC_CASINO CA
                                            ON CC.ID_CASINO = CA.ID_CASINO
                                        INNER JOIN PFSC_COLACION COL
                                            ON CC.ID_COLACION = COL.ID_COLACION
                                        INNER JOIN PFSC_TIPO_SERVICIO SE
                                            ON CC.ID_TIPO_SERVICIO = SE.ID_TIPO_SERVICIO
                                        INNER JOIN PFSC_PLANTA PA
                                            ON CA.ID_PLANTA = PA.ID_PLANTA
                                ORDER BY CC.ID_COLABORADOR ASC) INNER_QUERY
                        WHERE ROWNUM <= :PAGE * 10) ${rnum}`,
        [req.params.page],
        res)
}

const getBy = async (req, res) => {
    db.dbSelectResult(
        control_casino.db_casino,
        `SELECT CC.ID_COLABORADOR,
                    CO.NOMBRE NOMBRE_COLABORADOR,
                    CC.ESTADO,
                    CC.MAXTIPOCOLACION,
                    CC.ID_CASINO,
                    CA.NOMBRE NOMBRE_CASINO,
                    CA.ID_PLANTA,
                    PA.NOMBRE NOMBRE_PLANTA,
                    CC.ID_COLACION,
                    COL.NOMBRE NOMBRE_COLACION,
                    CC.ID_TIPO_SERVICIO,
                    SE.NOMBRE NOMBRE_TIPO_SERVICIO,
                    CC.CREADO_POR,
                    CC.FECHA_CREACION,
                    CC.MODIFICADO_POR,
                    CC.FECHA_MODIFICACION
                FROM PFSC_COLABORADOR_COLACION CC
                INNER JOIN PFSC_COLABORADOR CO ON CC.ID_COLABORADOR = CO.ID_COLABORADOR
                INNER JOIN PFSC_CASINO CA ON CC.ID_CASINO = CA.ID_CASINO
                INNER JOIN PFSC_COLACION COL ON CC.ID_COLACION = COL.ID_COLACION
                INNER JOIN PFSC_TIPO_SERVICIO SE ON CC.ID_TIPO_SERVICIO = SE.ID_TIPO_SERVICIO
                INNER JOIN PFSC_PLANTA PA ON CA.ID_PLANTA = PA.ID_PLANTA
            WHERE CC.ID_COLABORADOR = :IDBV
            ORDER BY CC.ID_COLABORADOR ASC`,
        [req.params.id],
        res)
}


const upd = async (req, res) => {
    db.dbCrud(
        control_casino.db_casino,
        `UPDATE PFSC_COLABORADOR_COLACION
                SET ESTADO           = :DV_ESTADO,
                    MAXTIPOCOLACION  = :DV_MAXTIPOCOLACION,
                    ID_TIPO_SERVICIO = :DV_ID_TIPO_SERVICIO,
                    MODIFICADO_POR   = :DV_MODIFICADO_POR,
                    FECHA_MODIFICACION = SYSDATE
                WHERE ID_COLABORADOR = :DV_ID_COLABORADOR AND
                    ID_CASINO      = :DV_ID_CASINO AND
                    ID_COLACION    = :DV_ID_COLACION`,
        [
            req.body.estado,
            req.body.maxtipocolacion,
            req.body.idtiposervicio,
            req.body.modificadopor,
            req.body.idcolaborador,
            req.body.idcasino,
            req.body.idcolacion
        ], res)
}

const del = async (req, res) => {
    db.dbCrud(
        control_casino.db_casino,
        `DELETE PFSC_COLABORADOR_COLACION
                    WHERE	   ID_COLABORADOR = :DV_ID_COLABORADOR
                        AND ID_CASINO = :DV_ID_CASINO
                        AND ID_COLACION = :DV_ID_COLACION`,
        [
            req.body.idcolaborador,
            req.body.idcasino,
            req.body.idcolacion
        ], res)
}

const add = async (req, res) => {
    db.dbCrud(
        control_casino.db_casino,
        `INSERT INTO PFSC_COLABORADOR_COLACION( ID_COLABORADOR,
                                                    ESTADO,
                                                    MAXTIPOCOLACION,
                                                    ID_CASINO,
                                                    ID_COLACION,
                                                    ID_TIPO_SERVICIO,
                                                    CREADO_POR,
                                                    FECHA_CREACION)
            VALUES (:DV_ID_COLABORADOR,
                    :DV_ESTADO,
                    :DV_MAXTIPOCOLACION,
                    :DV_ID_CASINO,
                    :DV_ID_COLACION,
                    :DV_ID_TIPO_SERVICIO,
                    :DV_CREADO_POR,
                    SYSDATE)`,
        [
            req.body.idcolaborador,
            req.body.estado,
            req.body.maxtipocolacion,
            req.body.idcasino,
            req.body.idcolacion,
            req.body.idtiposervicio,
            req.body.creadopor
        ], res)
}

module.exports = {
    get,
    getBy,
    upd,
    add,
    del
}