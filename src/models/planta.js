const db = require(__dirDBOracle)
const control_casino = require(__dirDB)

const get = async (req, res) => {
    db.dbSelectResult(
        control_casino.db_casino,
        `SELECT ID_PLANTA,
                    ESTADO,
                    NOMBRE,
                    CREADO_POR,
                    FECHA_CREACION,
                    MODIFICADO_POR,
                    FECHA_MODIFICACION
                FROM PFSC_PLANTA`,
        [],
        res)
}

const getBy = async (req, res) => {
    db.dbSelectResult(
        control_casino.db_casino,
        `SELECT ID_PLANTA,
                    ESTADO,
                    NOMBRE,
                    CREADO_POR,
                    FECHA_CREACION,
                    MODIFICADO_POR,
                    FECHA_MODIFICACION
                FROM PFSC_PLANTA`,
        [req.params.id],
        res)
}

const upd = async (req, res) => {
    db.dbCrud(
        control_casino.db_casino,
        `UPDATE PFSC_PLANTA
                SET ESTADO = :ESTADO,
                    NOMBRE = :NOMBRE,
                    MODIFICADO_POR = :MODIFICADO_POR,
                    FECHA_MODIFICACION = SYSDATE
                WHERE ID_PLANTA = :ID_PLANTA`,
        [
            req.body.estado,
            req.body.nombre,
            req.body.modificado_por,
            req.body.id_planta,
        ], res)
}

const del = async (req, res) => {
    db.dbCrud(
        control_casino.db_casino,
        `DELETE PFSC_PLANTA
                    WHERE	   ID_PLANTA = :ID_PLANTA`,
        [
            req.body.idcolaborador,
        ], res)
}

const add = async (req, res) => {
    db.dbCrud(
        control_casino.db_casino,
        `INSERT INTO PFSC_PLANTA(ID_PERFIL,
                                    ESTADO,
                                    NOMBRE,
                                    CREADO_POR,
                                    FECHA_CREACION)
                VALUES (:ID_PERFIL,
                        :ESTADO,
                        :NOMBRE,
                        :CREADO_POR,
                        SYSDATE)`,
        [
            req.body.id_perfil,
            req.body.estado,
            req.body.nombre,
            req.body.creado_por
        ], res)
}

module.exports = {
    get,
    getBy,
    upd,
    add,
    del
}