const db = require(__dirDBOracle)
const control_casino = require(__dirDB)

const get = async (req, res) => {
    let rnum = `WHERE RNUM > 10 * (${req.params.page} - 1)`
    db.dbSelectResult(
        control_casino.db_casino,
        `SELECT *
                FROM (SELECT INNER_QUERY.*, ROWNUM RNUM
                        FROM (  SELECT ID_COLABORADOR,
                                    ESTADO,
                                    NOMBRE,
                                    CLAVE,
                                    CODIGO,
                                    CORREO,
                                    CREDENCIAL,
                                    DEPARTAMENTO,
                                    EMPRESA,
                                    GERENCIA,
                                    NOMBRE_EMPRESA,
                                    NRO_TARJETA_VISITA,
                                    RUTSOLICITANTE,
                                    UBICACION_CONTABLE,
                                    UBICACION_FISICA,
                                    ID_AREA,
                                    ID_PERFIL,
                                    ID_CASINO,
                                    CREADO_POR,
                                    FECHA_CREACION,
                                    MODIFICADO_POR,
                                    FECHA_MODIFICACION
                                FROM PFSC_COLABORADOR ) INNER_QUERY
                        WHERE ROWNUM <= :PAGE * 10) ${rnum}`,
        [req.params.page],
        res)
}

const getBy = async (req, res) => {
    db.dbSelectResult(
        control_casino.db_casino,
        `SELECT ID_COLABORADOR,
                ESTADO,
                NOMBRE,
                CLAVE,
                CODIGO,
                CORREO,
                CREDENCIAL,
                DEPARTAMENTO,
                EMPRESA,
                GERENCIA,
                NOMBRE_EMPRESA,
                NRO_TARJETA_VISITA,
                RUTSOLICITANTE,
                UBICACION_CONTABLE,
                UBICACION_FISICA,
                ID_AREA,
                ID_PERFIL,
                ID_CASINO,
                CREADO_POR,
                FECHA_CREACION,
                MODIFICADO_POR,
                FECHA_MODIFICACION
            FROM PFSC_COLABORADOR
            WHERE ID_COLABORADOR = :ID_COLABORADOR`,
        [req.params.id],
        res)
}


const upd = async (req, res) => {
    db.dbCrud(
        control_casino.db_casino,
        `UPDATE PFSC_COLABORADOR_COLACION
                SET ESTADO              = :ESTADO,
                    NOMBRE              = :NOMBRE,
                    CLAVE               = :CLAVE,
                    CODIGO              = :CODIGO,
                    CORREO              = :CORREO,
                    CREDENCIAL          = :CREDENCIAL,
                    DEPARTAMENTO        = :DEPARTAMENTO,
                    EMPRESA             = :EMPRESA,
                    GERENCIA            = :GERENCIA,
                    NOMBRE_EMPRESA      = :NOMBRE_EMPRESA,
                    NRO_TARJETA_VISITA  = :NRO_TARJETA_VISITA,
                    RUTSOLICITANTE      = :RUTSOLICITANTE,
                    UBICACION_CONTABLE  = :UBICACION_CONTABLE,
                    UBICACION_FISICA    = :UBICACION_FISICA,
                    ID_AREA             = :ID_AREA,
                    ID_PERFIL           = :ID_PERFIL,
                    ID_CASINO           = :ID_CASINO,
                    MODIFICADO_POR      = :MODIFICADO_POR,
                    FECHA_MODIFICACION  = SYSDATE
                WHERE ID_COLABORADOR = :DV_ID_COLABORADOR`,
        [
            req.body.estado,
            req.body.nombre,
            req.body.clave,
            req.body.codigo,
            req.body.correo,
            req.body.credencial,
            req.body.departamento,
            req.body.empresa,
            req.body.gerencia,
            req.body.nombre_empresa,
            req.body.nro_tarjeta_visita,
            req.body.rutsolicitante,
            req.body.ubicacion_contable,
            req.body.ubicacion_fisica,
            req.body.id_area,
            req.body.id_perfil,
            req.body.id_casino,
            req.body.modificado_por,
            req.body.fecha_modificacion,
            req.body.id_colaborador,
        ], res)
}

const del = async (req, res) => {
    db.dbCrud(
        control_casino.db_casino,
        `DELETE PFSC_COLABORADOR
                    WHERE	   ID_COLABORADOR = :DV_ID_COLABORADOR`,
        [
            req.body.idcolaborador
        ], res)
}

const add = async (req, res) => {
    db.dbCrud(
        control_casino.db_casino,
        `INSERT INTO PFSC_COLABORADOR(  ID_COLABORADOR,
                                            ESTADO,
                                            NOMBRE,
                                            CLAVE,
                                            CODIGO,
                                            CORREO,
                                            CREDENCIAL,
                                            DEPARTAMENTO,
                                            EMPRESA,
                                            GERENCIA,
                                            NOMBRE_EMPRESA,
                                            NRO_TARJETA_VISITA,
                                            RUTSOLICITANTE,
                                            UBICACION_CONTABLE,
                                            UBICACION_FISICA,
                                            ID_AREA,
                                            ID_PERFIL,
                                            ID_CASINO,
                                            CREADO_POR,
                                            FECHA_CREACION)
                    VALUES (NVL(MAX(ID_COLABORADOR) + 1, 1),
                            :ESTADO,
                            :NOMBRE,
                            :CLAVE,
                            :CODIGO,
                            :CORREO,
                            :CREDENCIAL,
                            :DEPARTAMENTO,
                            :EMPRESA,
                            :GERENCIA,
                            :NOMBRE_EMPRESA,
                            :NRO_TARJETA_VISITA,
                            :RUTSOLICITANTE,
                            :UBICACION_CONTABLE,
                            :UBICACION_FISICA,
                            :ID_AREA,
                            :ID_PERFIL,
                            :ID_CASINO,
                            :CREADO_POR,
                            SYSDATE)`,
        [
            req.body.estado,
            req.body.nombre,
            req.body.clave,
            req.body.codigo,
            req.body.correo,
            req.body.credencial,
            req.body.departamento,
            req.body.empresa,
            req.body.gerencia,
            req.body.nombre_empresa,
            req.body.nro_tarjeta_visita,
            req.body.rutsolicitante,
            req.body.ubicacion_contable,
            req.body.ubicacion_fisica,
            req.body.id_area,
            req.body.id_perfil,
            req.body.id_casino,
            req.body.creado_por
        ], res)
}

module.exports = {
    get,
    getBy,
    upd,
    add,
    del
}