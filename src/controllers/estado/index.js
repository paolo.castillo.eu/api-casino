const model_estado = require(__baseDir + '/src/models/estado');

const get = (req, res) => model_estado.get(req, res)
const getBy = (req, res) => model_estado.getBy(req, res)

module.exports = {
    get,
    getBy,
}