const model_casino = require(__baseDir + '/src/models/casino')

const get = (req, res) => model_casino.get(req, res)
const getBy = (req, res) => model_casino.getBy(req, res)
const add = (req, res) => model_casino.add(req, res)
const upd = (req, res) => model_casino.upd(req, res)
const del = (req, res) => model_casino.del(req, res)

module.exports = {
    get,
    getBy,
    upd,
    add,
    del
}

// endpoints controllers casino