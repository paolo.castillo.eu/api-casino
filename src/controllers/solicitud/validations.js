const { body, param, validationResult } = require('express-validator')

exports.check_id = [
    param('id')
        .exists().withMessage('Id es requerido')
        .not().isEmpty().bail()
        .isNumeric().withMessage('Debe ser numerico'),
    (req, res, next) => {
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            console.log(req.body)
            const values = req.body
            const validation = errors.array()
            return res.status(422).json({ validation: validation, values: values })
        } else next()
    }
]

exports.check_add = [
    body('id_solicitud', 'Ingrese numero de solicitud').exists().isNumeric(),
    body('rut', 'Ingrese rut de la solicitud')
        .custom((value, { req }) => {
            if (rutValidate(value) == False) {
                return false
            }
            return true
        }).withMessage('Rut no es valido')
        .exists()
        .not().isEmpty().bail()
        .isLength({ max: 15 }),
    body('modificado', 'Ingrese usuario que realizara una acción').not().isEmpty().bail().exists().isNumeric(),
    (req, res, next) => {
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            console.log(req.body)
            const values = req.body
            const validation = errors.array()
            return res.status(422).json({ validation: validation, values: values })
        } else next()
    }
]

const rutValidate = (text) => {
    text = text.replace("‐", "-");
    if (!/^[0-9]+[-|‐]{1}[0-9kK]{1}$/.test(rut))
        return false
    const tmp = text.split('-')
    const digv = tmp[1]
    const rut = tmp[0]
    if (digv == 'K') digv = 'k'

    return (dv(rut) == digv)
}

const dv = (T) => {
    var M = 0, S = 1;
    for (; T; T = Math.floor(T / 10))
        S = (S + T % 10 * (9 - M++ % 6)) % 11
    return S ? S - 1 : 'k'
}
