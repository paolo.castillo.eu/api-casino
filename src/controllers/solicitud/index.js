const model_solicitud = require(__baseDir + '/src/models/solicitud')

const getBy = (req, res) => model_solicitud.getBy(req, res)
const add = (req, res) => model_solicitud.add(req, res)

module.exports = {
    getBy,
    add
}
