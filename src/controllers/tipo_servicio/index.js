const model_tipo_servicio = require(__baseDir + '/src/models/servicio')

const get = (req, res) => model_tipo_servicio.get(req, res)
const getBy = (req, res) => model_tipo_servicio.getBy(req, res)
const upd = (req, res) => model_tipo_servicio.upd(req, res)

module.exports = {
    get,
    getBy,
    upd
}
