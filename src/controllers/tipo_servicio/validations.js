const { body, param, validationResult } = require('express-validator')

exports.check_id = [
    param('id', 'Id es requerido')
        .exists()
        .isNumeric(),
    (req, res, next) => {
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            console.log(req.body)
            const values = req.body
            const validation = errors.array()
            return res.status(422).json({ validation: validation, values: values })
        } else next()
    }
]

exports.check_upd = [
    body('nombre', 'Ingrese un nombre').not().isEmpty(),
    body('modificado_por', 'Ingrese un modificador').not().isEmpty(),
    body('id_tipo_servicio', 'Ingrese el tipo de servicio').not().isEmpty().isNumeric(),
    (req, res, next) => {
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            console.log(req.body)
            const values = req.body
            const validation = errors.array()
            return res.status(422).json({ validation: validation, values: values })
        } else next()
    }
]
