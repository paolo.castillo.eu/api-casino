const model_colaborador = require(__baseDir + '/src/models/colaborador')

const get = (req, res) => model_colaborador.get(req, res)
const getBy = (req, res) => model_colaborador.getBy(req, res)
const add = (req, res) => model_colaborador.add(req, res)
const upd = (req, res) => model_colaborador.upd(req, res)
const del = (req, res) => model_colaborador.del(req, res)

module.exports = {
    get,
    getBy,
    upd,
    add,
    del
}