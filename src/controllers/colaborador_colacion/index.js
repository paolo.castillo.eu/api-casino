const model_colaborador_colacion = require(__baseDir + '/src/models/colaborador_colacion')

const get = (req, res) => model_colaborador_colacion.get(req, res)
const getBy = (req, res) => model_colaborador_colacion.getBy(req, res)
const add = (req, res) => model_colaborador_colacion.add(req, res)
const upd = (req, res) => model_colaborador_colacion.upd(req, res)
const del = (req, res) => model_colaborador_colacion.del(req, res)

module.exports = {
    get,
    getBy,
    upd,
    add,
    del
}