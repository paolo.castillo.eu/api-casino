const { body, param, validationResult } = require('express-validator')

exports.check_id = [
    param('id', 'Id es requerido')
        .exists()
        .isNumeric(),
    (req, res, next) => {
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            console.log(req.body)
            const values = req.body
            const validation = errors.array()
            return res.status(422).json({ validation: validation, values: values })
        } else next()
    }
]

exports.check_add = [
    body('idcolaborador', 'Ingrese un colaborador').not().isEmpty().bail().isNumeric(),
    body('maxtipocolacion', 'Ingrese numero de colación').not().isEmpty().bail().isNumeric(),
    body('idcasino', 'Ingrese un casino').not().isEmpty().bail().isNumeric(),
    body('idcolacion', 'Ingrese una colación').not().isEmpty().bail().isNumeric(),
    body('idtiposervicio', 'Ingrese un tipo de sevicio').not().isEmpty().bail().isNumeric(),
    body('creadopor', 'Ingrese un creador').not().isEmpty().bail().isNumeric(),
    (req, res, next) => {
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            console.log(req.body)
            const values = req.body
            const validation = errors.array()
            return res.status(422).json({ validation: validation, values: values })
        } else next()
    }
]

exports.check_upd = [
    body('maxtipocolacion', 'Ingrese numero de colación').not().isEmpty().bail().isNumeric(),
    body('idtiposervicio', 'Ingrese un tipo de sevicio').not().isEmpty().bail().isNumeric(),
    body('modificadopor', ' Ingrese un modificador').not().isEmpty().bail().isNumeric(),
    body('idcolaborador', 'Ingrese un colaborador').not().isEmpty().bail().isNumeric(),
    body('idcasino', 'Ingrese un casino').not().isEmpty().bail().isNumeric(),
    body('idcolacion', 'Ingrese una colación').not().isEmpty().bail().isNumeric(),
    (req, res, next) => {
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            console.log(req.body)
            const values = req.body
            const validation = errors.array()
            return res.status(422).json({ validation: validation, values: values })
        } else next()
    }
]

exports.check_del = [
    body('idcolaborador', 'Ingrese un colaborador').not().isEmpty().bail().isNumeric(),
    body('idcasino', 'Ingrese un casino').not().isEmpty().bail().isNumeric(),
    body('idcolacion', 'Ingrese una colación').not().isEmpty().bail().isNumeric(),
    (req, res, next) => {
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            console.log(req.body)
            const values = req.body
            const validation = errors.array()
            return res.status(422).json({ validation: validation, values: values })
        } else next()
    }
]