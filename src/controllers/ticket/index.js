const model_ticket = require(__baseDir + '/src/models/ticket')

const get = (req, res) => model_ticket.get(req, res)
const getTicket = (req, res) => model_ticket.getTicket(req, res)

module.exports = {
    get,
    getTicket
}
