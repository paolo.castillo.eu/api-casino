const model_planta = require(__baseDir + '/src/models/planta')

const get = (req, res) => model_planta.get(req, res)
const getBy = (req, res) => model_planta.getBy(req, res)
const add = (req, res) => model_planta.add(req, res)
const upd = (req, res) => model_planta.upd(req, res)
const del = (req, res) => model_planta.del(req, res)

module.exports = {
    get,
    getBy,
    upd,
    add,
    del
}

// endpoints controllers casino