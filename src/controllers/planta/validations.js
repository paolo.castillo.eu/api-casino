const { body, param, validationResult } = require('express-validator')


exports.check_id = [
    param('id', 'Id es requerido')
        .exists()
        .isNumeric(),
    (req, res, next) => {
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            console.log(req.body)
            const values = req.body
            const validation = errors.array()
            return res.status(422).json({ validation: validation, values: values })
        } else next()
    }
];

exports.check_add = [
    body('id_casino', 'Ingrese un código para el casino').not().isEmpty().bail().isNumeric(),
    body('capacidad', 'Debe ingresar la capacidad').not().isEmpty().bail().isNumeric().withMessage('Debe ser numerico'),
    body('correo_admin', 'Ingrese un correo valido').not().isEmpty().bail().isEmail(),
    body('correo_admin_pf', 'Ingrese un correo valido').not().isEmpty().bail().isEmail(),
    body('nombre', 'El nombre es obligatorio').not().isEmpty(),
    body('terminales', 'Ingrese un terminal').not().isEmpty().bail().isNumeric(),
    body('id_planta', 'La planta es obligatorio').not().isEmpty().bail().isNumeric(),
    body('creado_por', 'Ingrese un creador').not().isEmpty().bail().isNumeric(),
    body('correo_notif_adm', 'Ingrese un correo valido').not().isEmpty().bail().isEmail(),
    body('correo_notif_rrhh', 'Ingrese un correo valido').not().isEmpty().bail().isEmail(),
    body('correo_notif_seguridad', 'Ingrese un correo valido').not().isEmpty().bail().isEmail(),
    body('correo_notif_marca', 'Ingrese un correo valido').not().isEmpty().bail().isEmail(),
    (req, res, next) => {
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            console.log(req.body)
            const values = req.body
            const validation = errors.array()
            return res.status(422).json({ validation: validation, values: values })
        } else next()
    }
]

exports.check_upd = [
    body('estado', '').not().isEmpty(),
    body('nombre', '').not().isEmpty(),
    body('modificado_por', '').not().isEmpty(),
    body('id_planta', '').not().isEmpty().bail().isNumeric(),
    (req, res, next) => {
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            console.log(req.body)
            const values = req.body
            const validation = errors.array()
            return res.status(422).json({ validation: validation, values: values })
        } else next()
    }
]

exports.check_del = [
    body('id_casino', 'Ingrese un código para el casino').not().isEmpty().bail().isNumeric(),
    (req, res, next) => {
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            console.log(req.body)
            const values = req.body
            const validation = errors.array()
            return res.status(422).json({ validation: validation, values: values })
        } else next()
    }
]