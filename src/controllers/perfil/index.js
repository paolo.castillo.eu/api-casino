const model_perfil = require(__baseDir + '/src/models/perfil')

const get = (req, res) => model_perfil.get(req, res)
const getBy = (req, res) => model_perfil.getBy(req, res)

module.exports = {
    get,
    getBy,
}