const { body, param, validationResult } = require('express-validator')

exports.check_id = [
    param('id', 'Id es requerido')
        .exists()
        .isNumeric(),
    (req, res, next) => {
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            console.log(req.body)
            const values = req.body
            const validation = errors.array()
            return res.status(422).json({ validation: validation, values: values })
        } else next()
    }
];

exports.check_add = [
    body('idcolaborador', 'Ingrese un colaborador').not().isEmpty(),
    body('estado', 'Ingrese un estado').not().isEmpty(),
    body('maxtipocolacion', 'Ingrese un maximo de colación').not().isEmpty(),
    body('idcasino', 'Ingrese un casino').not().isEmpty().bail().isNumeric(),
    body('idcolacion', 'Ingrese una colación').not().isEmpty().bail().isNumeric(),
    body('idtiposervicio', 'Ingrese el tipo de servicio').not().isEmpty().bail().isNumeric(),
    body('creadopor', 'Ingrese un creador').not().isEmpty(),
    (req, res, next) => {
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            console.log(req.body)
            const values = req.body
            const validation = errors.array()
            return res.status(422).json({ validation: validation, values: values })
        } else next()
    }
]

exports.check_upd = [
    body('estado', 'Ingrese un estado').not().isEmpty(),
    body('maxtipocolacion', 'Ingrese un maximo de colación').not().isEmpty().isNumeric(),
    body('idtiposervicio', 'Ingrese el tipo de servicio').not().isEmpty(),
    body('modificadopor', 'Ingrese un modificador').not().isEmpty(),
    body('idcolaborador', 'Ingrese un colaborador').not().isEmpty(),
    body('idcasino', 'Ingrese un casino').not().isEmpty().bail().isNumeric(),
    body('idcolacion', 'Ingrese una colación').not().isEmpty().bail().isNumeric(),
    (req, res, next) => {
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            console.log(req.body)
            const values = req.body
            const validation = errors.array()
            return res.status(422).json({ validation: validation, values: values })
        } else next()
    }
]

exports.check_del = [
    body('idcolaborador').not().isEmpty(),
    body('idcasino').not().isEmpty(),
    body('idcolacion').not().isEmpty(),
    (req, res, next) => {
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            console.log(req.body)
            const values = req.body
            const validation = errors.array()
            return res.status(422).json({ validation: validation, values: values })
        } else next()
    }
]