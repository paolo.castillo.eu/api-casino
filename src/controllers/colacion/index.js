const model_colacion = require(__baseDir + '/src/models/colacion');

const get = (req, res) => model_colacion.get(req, res)
const getBy = (req, res) => model_colacion.getBy(req, res)
const add = (req, res) => model_colacion.add(req, res)
const upd = (req, res) => model_colacion.upd(req, res)
const del = (req, res) => model_colacion.del(req, res)

module.exports = {
    get,
    getBy,
    upd,
    add,
    del
}