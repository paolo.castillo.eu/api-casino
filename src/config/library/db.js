const oracledb = require('oracledb')

let connection

const dbSelectResult = async (db, query, bind, res) => {
    connection = await oracledb.getConnection(db)
    connection.execute(query, bind,{outFormat: oracledb.OUT_FORMAT_OBJECT}, (err, result) => {
        if (err){
            res.status(200).json({ message: 'Error listar query ', error: err + "JJ" })
        }else{
            res.json({ results:result.rows, status:200})
        }
        if (connection) {
            try {
                connection.close()
            } catch (error) {
                res.status(200).json({ message: 'Error desconectar query ', error: err + "JJ" })
            }
        }
    })
}

const dbCrud = async (db, query, bind, res) => {
    connection = await oracledb.getConnection(db)
    connection.execute(query, bind, { autoCommit: true }, (err, result) => {
        if (err) {
            res.status(200).json({ message: 'Error crud ', error: err + "JJ" })
        } else {
            res.json({ results: result, status:200 })
        }
        if (connection) {
            try {
                connection.close()
            } catch (error) {
                res.status(200).json({ message: 'Error desconectar ', error: err + "JJ" })
            }
        }
    })
}

const dbProcedure = async (db, query, bind, res) => {
    connection = await oracledb.getConnection(db)
    connection.execute(query, bind, (err, result) => {
        if (err) {
            res.status(200).json({ message: 'Error cursor ', error: err + "JJ" })
        } else {
            res.json({ results:result, status:200 })
        }
        if (connection) {
            try {
                connection.close()
            } catch (error) {
                res.status(200).json({ message: 'Error desconectar ', error: err + "JJ" })
            }
        }
    })
}

const dbCursor = async (db, query, bind, res) => {
    connection = await oracledb.getConnection(db)
    connection.execute(query, bind,{ outFormat: oracledb.OUT_FORMAT_OBJECT }, (err, result) => {
        if (err) {
            res.status(200).json({ message: 'Error cursor ', error: err + "JJ" })
        } else {
            const resultSet = result.outBinds.CURSOR
            resultSet.getRows().then(rows => {
                res.json({ results : rows, status:200 })
            })
        }
        if (connection) {
            try {
                connection.close()
            } catch (error) {
                res.status(200).json({ message: 'Error desconectar ', error: err + "JJ" })
            }
        }
    })
}

module.exports = {
    dbSelectResult,
    dbCrud,
    dbProcedure,
    dbCursor
}