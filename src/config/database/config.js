const db_apps = JSON.parse(process.env.db_apps)
const db_casino = JSON.parse(process.env.db_casino)

module.exports = {
    db_apps,
    db_casino,
}