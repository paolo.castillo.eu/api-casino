const express = require("express")
const router = express.Router()

const casino = require("./casino")
router.use("/casino", casino)

const colaborador_colacion = require("./colaborador_colacion")
router.use("/colaborador/colacion", colaborador_colacion)

const colaborador = require("./colaborador")
router.use("/colaborador", colaborador)

const colacion = require("./colacion")
router.use("/colacion", colacion)

const estado = require("./estado")
router.use("/estado", estado)

const perfil = require("./perfil")
router.use("/perfil", perfil)

const planta = require("./planta")
router.use("/planta", planta)

const solicitud = require("./solicitud")
router.use("/solicitud", solicitud)

const ticket = require("./ticket")
router.use("/ticket", ticket)

const tipo_servicio = require("./tipo_servicio")
router.use("/tipo/servicio", tipo_servicio)

const usuario = require("./usuario")
router.use("/usuario", usuario)

router.get("/", (request, response) => {
    response.status(200).json({ message: "Router connected ok!" })
})

module.exports = router