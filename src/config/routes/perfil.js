const rt_perfil = require('express').Router()
const ctr_perfil = require(__baseDir + '/src/controllers/perfil');
const validate = require(__baseDir + '/src/controllers/perfil/validations');

rt_perfil.get('/get', ctr_perfil.get)
rt_perfil.get('/get/:id', validate.check_id , ctr_perfil.getBy)

module.exports = rt_perfil