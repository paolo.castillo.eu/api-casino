const rt_solicitud = require('express').Router()
const ctr_solicitud = require(__baseDir + '/src/controllers/solicitud');
const validate = require(__baseDir + '/src/controllers/solicitud/validations')

rt_solicitud.get('/get/:id', validate.check_id, ctr_solicitud.getBy)
rt_solicitud.post('/add', validate.check_add, ctr_solicitud.add)


module.exports = rt_solicitud