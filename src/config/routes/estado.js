const rt_estado = require('express').Router()
const ctr_estado = require(__baseDir + '/src/controllers/estado');
const validate = require(__baseDir + '/src/controllers/estado/validations');

rt_estado.get('/get', ctr_estado.get)
rt_estado.get('/get/:id', validate.check_id , ctr_estado.getBy)

module.exports = rt_estado