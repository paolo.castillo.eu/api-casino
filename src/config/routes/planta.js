const rt_planta = require('express').Router()
const ctr_planta = require(__baseDir + '/src/models/planta');

rt_planta.get('/get', (req, res) => ctr_planta.get(req, res))
rt_planta.get('/get/:id', (req, res) => ctr_planta.getBy(req, res))
rt_planta.post('/add', (req, res) => ctr_planta.add(req, res))
rt_planta.post('/upd', (req, res) => ctr_planta.upd(req, res))
rt_planta.post('/del', (req, res) => ctr_planta.del(req, res))

module.exports = rt_planta