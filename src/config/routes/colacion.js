const rt_colacion = require('express').Router()
const ctr_colacion = require(__baseDir + '/src/controllers/colacion');
const validate = require(__baseDir + '/src/controllers/colacion/validations')

rt_colacion.get('/get', ctr_colacion.get)
rt_colacion.get('/get/:id', validate.check_id, ctr_colacion.getBy)
rt_colacion.post('/add', validate.check_add, ctr_colacion.add)
rt_colacion.post('/upd', validate.check_upd, ctr_colacion.upd)
rt_colacion.post('/del', validate.check_del, ctr_colacion.del)

module.exports = rt_colacion