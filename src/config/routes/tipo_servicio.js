const rt_servicio = require('express').Router()
const ctr_servicio = require(__baseDir + '/src/controllers/tipo_servicio');
const validations = require(__baseDir + '/src/controllers/tipo_servicio/validations');

rt_servicio.get('/get', ctr_servicio.get )
rt_servicio.get('/get/:id', validations.check_id, ctr_servicio.getBy )
rt_servicio.post('/upd', validations.check_upd, ctr_servicio.upd )

module.exports = rt_servicio