const rt_ticket = require('express').Router()
const ctr_ticket = require(__baseDir + '/src/controllers/ticket');

rt_ticket.get('/get', ctr_ticket.get)
rt_ticket.get('/getTicket', ctr_ticket.getTicket)


module.exports = rt_ticket