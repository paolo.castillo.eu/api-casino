const rt_casino = require('express').Router()
const ctr_casino = require(__baseDir + '/src/controllers/casino')
const validate = require(__baseDir + '/src/controllers/casino/validations')

rt_casino.get('/get', (req, res) => ctr_casino.get(req, res))
rt_casino.get('/get/:id', validate.check_id, ctr_casino.getBy)
rt_casino.post('/add', validate.check_add, ctr_casino.add)
rt_casino.post('/upd', validate.check_upd, ctr_casino.upd)
rt_casino.post('/del', validate.check_del, ctr_casino.del)

module.exports = rt_casino