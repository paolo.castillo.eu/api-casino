const rt_colaborador_colacion = require('express').Router()
const ctr_colaborador_colacion = require(__baseDir + '/src/controllers/colaborador_colacion');
const validate = require(__baseDir + '/src/controllers/colaborador_colacion/validations')

rt_colaborador_colacion.get('/get/colacion/:page', (req, res) => ctr_colaborador_colacion.get(req, res))
rt_colaborador_colacion.get('/get/:id', validate.check_id, ctr_colaborador_colacion.getBy)
rt_colaborador_colacion.post('/add', validate.check_add, ctr_colaborador_colacion.add)
rt_colaborador_colacion.post('/upd', validate.check_upd, ctr_colaborador_colacion.upd)
rt_colaborador_colacion.post('/del', validate.check_del, ctr_colaborador_colacion.del)

module.exports = rt_colaborador_colacion