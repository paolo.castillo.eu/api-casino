const rt_colaborador = require('express').Router()
const ctr_colaborador = require(__baseDir + '/src/controllers/colaborador');
const validate = require(__baseDir + '/src/controllers/colaborador/validations')

rt_colaborador.get('/get/colaborador/:page', (req, res) => ctr_colaborador.get(req, res))
rt_colaborador.get('/get/:id', validate.check_id, ctr_colaborador.getBy)
rt_colaborador.post('/add', validate.check_add, ctr_colaborador.add)
rt_colaborador.post('/upd', validate.check_add, ctr_colaborador.add)
rt_colaborador.post('/del', validate.check_del, ctr_colaborador.del)

module.exports = rt_colaborador