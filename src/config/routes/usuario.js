const rt_usuario = require('express').Router()
const axios = require('axios');
var https = require('https');

const agent = new https.Agent({
    rejectUnauthorized: false
});

rt_usuario.get("/test", (req, res, next) => {
    // rt_usuario.get("/test", async (req, res, next) => {
    // const users = await axios.get("https://jsonplaceholder.typicode.com/users", { httpsAgent: agent })
    // res.status(200).json({
    //     layout: "main",
    //     users: users.data
    // })
    axios.get("https://jsonplaceholder.typicode.com/users", { httpsAgent: agent })
        .then(data => res.status(200).json(data.data))
        .catch(err => next(err));
})

module.exports = rt_usuario