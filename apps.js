const app = require('./server')

// Port
const port = process.env.PORT || 8080
app.listen(port, () => {
    console.log("Running app on port port. Visit: http://localhost:" + port + "/")
})