global.__baseDir = __dirname
global.__dirDB = __baseDir + '/src/config/database/config'
global.__dirDBOracle = __baseDir + '/src/config/library/db'

// Middlewares
const cors = require('cors')
const morgan = require("morgan")
const helmet = require('helmet')
const express = require("express")
const bodyParser = require("body-parser")

const notFound = require('./src/middlewares/not-found')
const badRequest = require('./src/middlewares/bad-request')

const app = express()

require('dotenv').config()

app.use(cors())
app.use(helmet())
app.use(morgan("common"))
app.use(bodyParser.json({ limit: "50mb" }))
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true, parameterLimit: 50000 }))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use((request, response, next) => {
    response.header("Access-Control-Allow-Origin", "*")
    response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept,recording-session")
    next()
})

// Routes
const routes = require(__dirname + "/src/config/routes/")
app.use('/', routes)

// Middlewares
app.use(notFound)
app.use(badRequest)

module.exports = app